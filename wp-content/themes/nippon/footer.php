<?php global $data; ?>
	 <div class="footer">
   <div class="content">
      <div class="pro_policy pad_T10 pad_R10">
        <a href="<?php echo get_permalink( get_page_by_path( 'privacypolicy' ) ) ?>">≫個人情報保護方針</a>
      </div>
      <div class="footer_inner clearfix">
        <div class="foot_logo fll pad_R20">
          <a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/footer/footer_logo.png" alt="メープルツリーガーデン"></a>
        </div>
        <ul>
          <li>メープルツリーガーデン</li>
          <li>http://www.maple-tree-g.com</li>
          <li>〒285-0815　千葉県佐倉市城414-1</li>
          <li>電話＆FAX　043-371-0232</li>
        </ul>
      </div>
   </div> <!-- .content-->
  <div class="copy">
    <p class="content pad_T5">copy right (C) maple-tree-g.com All rights reserved.</p>
  </div>
 </div> <!-- footer-->
 <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
<script type="text/javascript">
  $('.bxslider').bxSlider({
    auto: true,
    autoControls: true
  });
</script>
</body>

</html>