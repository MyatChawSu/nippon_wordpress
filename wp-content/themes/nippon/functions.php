<?php
  global $data;
  require_once ('admin/index.php');


  if ( ! function_exists( 'misaki_setup' ) ) :
    function misaki_setup(){
      load_theme_textdomain( 'misaki', get_template_directory() . '/languages' );

      add_theme_support( 'automatic-feed-links' );

      add_theme_support( 'post-thumbnails' );
      set_post_thumbnail_size( 672, 372, true );
      add_image_size( 'misaki-full-width', 1038, 576, true );

      register_nav_menus( array(
        'primary'   => __( 'Top primary menu', 'misaki' ),
        'secondary' => __( 'Secondary menu in footer', 'misaki' ),
      ) );

      add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
      ) );

      add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
      ) );

    }
  endif;
  add_action( 'after_setup_theme', 'misaki_setup' );

  function misaki_content_width() {
    if ( is_attachment() && wp_attachment_is_image() ) {
      $GLOBALS['content_width'] = 810;
    }
  }
  add_action( 'template_redirect', 'misaki_content_width' );

  
    add_action( 'after_setup_theme', 'misaki_theme_setup' );

function misaki_theme_setup() {
  add_image_size( 'category-thumb', 210, 150); // 300 pixels wide (and unlimited height)

    add_image_size( 'page-thumb', 1000, 250); // (cropped)
    add_image_size( 'thumbnail', 372, 233); // (cropped)
  set_post_thumbnail_size( 'thumbnailsize' ,220, 138);

 function enqueue_scripts() {
    wp_enqueue_style( 'import-css', get_template_directory_uri() . '/common/css/import.css', false, false, 'all' );
    wp_enqueue_style( 'jquery.bxslider-css', get_template_directory_uri() . '/common/css/jquery.bxslider.css', false, false, 'all' );
    wp_enqueue_script( 'html5-js', get_template_directory_uri() .'/common/js/html5.js', array('jquery'));
    wp_enqueue_script( 'jquery-1.9.1-js', get_template_directory_uri() .'/common/js/jquery-1.9.1.js', array('jquery'));
    wp_enqueue_script( 'common-js', get_template_directory_uri() .'/common/js/common.js', array('jquery'));
    wp_enqueue_script( 'jquery.bxslider.min-js', get_template_directory_uri() .'/common/js/jquery.bxslider.min.js', array('jquery'));
    
  }
  
  add_action('wp_enqueue_scripts','enqueue_scripts');

  function misaki_widgets_init() {
    

    register_sidebar( array(
      'name'          => __( 'Primary Sidebar', 'twentyfourteen' ),
      'id'            => 'sidebar-1',
      'description'   => __( 'Main sidebar that appears on the left.', 'twentyfourteen' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h1 class="widget-title">',
      'after_title'   => '</h1>',
    ) );

    register_sidebar( array(
      'name'          => __( 'Primary Sidebar', 'twentyfourteen' ),
      'id'            => 'sidebar-1',
      'description'   => __( 'Main sidebar that appears on the left.', 'twentyfourteen' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h1 class="widget-title">',
      'after_title'   => '</h1>',
    ) );
  }
  // paganation//
function category_set_post_types( $query ){
    if( $query->is_category ):
        $query->set( 'post_type', 'any' );
    endif;
    return $query;
}
add_action( 'pre_get_posts', 'category_set_post_types' );

function qt_custom_breadcrumbs() {
 
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '<span class="glt">&gt;</span>'; // delimiter between crumbs
  $home = 'HOME'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . '' . single_cat_title('', false) . '' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end qt_custom_breadcrumbs() 

//img url//
function template_uri() {
    return get_template_directory_uri();
}
add_shortcode("template_uri", "template_uri");
/*p tag remove*/
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
/**
 * If more than one page exists, return TRUE.
 */
function show_posts_nav() {
  global $wp_query;
  return ($wp_query->max_num_pages > 1);
}
add_action( 'widgets_init', 'misaki_widgets_init' );

}
 function custom_posts_per_page( $query ) {

if ( $query->is_archive('project') ) {
    set_query_var('posts_per_page', 1);
}
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );

?>
