<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'treegarden');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '56?3K4(kc/aD53^e4|4+u5#v_;NY&oG_Q?^Ps6FfxWI}~*NxxVo,xNs!`XcA22t<');
define('SECURE_AUTH_KEY',  '8&j1/L3SY}duMj(yqilV.bX!RK8pSL;=A+H+ae!sGsp7<nDX%q>SY.0<kMH@cTro');
define('LOGGED_IN_KEY',    'M*FC sz?P+kVTQC&_8V+r&GQsvyK=H^ ,QpD[Z ]a} [j+w=qjY!L})SFH.W~pA`');
define('NONCE_KEY',        'Ho$=(0yAAL<St}:_ w ZeN9b^ma#Ecy9j`C_*Ru]=$/&eYb?d=TG*XhDerWS|I% ');
define('AUTH_SALT',        '+Q/)ABv>]TUYKA4CtsfVKuD?:95rKb^}nA9 ,A{1142w[YjBe;Gl+n*UayojuA:j');
define('SECURE_AUTH_SALT', 'cf8YN^P4O%Jw|c0(4<i4tOm/BJ29^X:N0V8MwM]Ix51YGq%1>`f8Ura1Y?aAc=.t');
define('LOGGED_IN_SALT',   'M(g@=E>kIo^=ea@^bj.cUeKDD&B5PA]!8QeAG?, :_!6id$-]SWUoO:52L@xtn~u');
define('NONCE_SALT',       '}dcZm=C2c4ll3L];hgic6BCd7IG1r#=zms`gh%G@)-A8sNL-FuH=ykpr(7.x4LFd');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
